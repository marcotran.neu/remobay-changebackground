from PIL import Image
import streamlit as st
from PIL import Image
import PIL
import uuid
import pixellib
from pixellib.tune_bg import alter_bg
import io
import cv2
import os
import moviepy.editor as moviepy


title = st.title("REMOBAY")
page = st.selectbox("Assistant", ["Change background image", "Change background video"])

if page == "Change background image":
    upload_face = st.file_uploader("upload image ...")
    if upload_face is not None:
        image_face = Image.open(upload_face)
        st.image(image_face, caption="Uploaded", use_column_width=True)
        image_face_path = "data" + "/" + str(uuid.uuid1()) + ".jpg"
        image_face.save(image_face_path)
    upload_idcard_front = st.file_uploader("upload background...")
    if upload_idcard_front is not None:
        image_idcard_front = Image.open(upload_idcard_front)
        st.image(image_idcard_front, caption="Uploaded", use_column_width=True)
        image_idcard_front_path = "data" + "/" + str(uuid.uuid1()) + ".jpg"
        image_idcard_front.save(image_idcard_front_path)
    if st.button('Change background'):
        output_path = "output" + "/" + str(uuid.uuid1()) + ".jpg"
        change_bg = alter_bg()
        change_bg.load_pascalvoc_model("deeplabv3_xception_tf_dim_ordering_tf_kernels.h5")
        change_bg.change_bg_img(f_image_path=image_face_path, b_image_path=image_idcard_front_path,
                                output_image_name=output_path)
        image = Image.open(output_path)
        st.image(image, caption='image with change background')



if page == "Change background video":
    uploaded_file = st.file_uploader("Choose a video...", type=["mp4"])
    temporary_location = False
    if uploaded_file is not None:
        g = io.BytesIO(uploaded_file.read())  ## BytesIO Object
        temporary_location = "data" + "/" + str(uuid.uuid1()) + ".mp4"

        with open(temporary_location, 'wb') as out:  ## Open temporary file as bytes
            out.write(g.read())  ## Read bytes into file

        # close file
        out.close()
        video_file = open(temporary_location, 'rb')
        video_bytes = video_file.read()
        print('1 :',temporary_location)

        st.video(video_bytes)

    upload_bg = st.file_uploader("upload background...")
    if upload_bg is not None:
        image_bg = Image.open(upload_bg)
        st.image(image_bg, caption="Uploaded", use_column_width=True)
        image_bg_path = "data" + "/" + str(uuid.uuid1()) + ".jpg"
        image_bg.save(image_bg_path)

    if st.button('Change background'):
        output_video = "output" + "/" + str(uuid.uuid1()) + ".mp4"
        change_bg = alter_bg(model_type="pb")
        change_bg.load_pascalvoc_model("xception_pascalvoc.pb")
        print('2 :', temporary_location)
        print('3 :', output_video)
        change_bg.change_video_bg(temporary_location, image_bg_path, frames_per_second=5,
                                  output_video_name=output_video, detect="person")
        output_2 = "output_processed" + "/" + "x_" +str(uuid.uuid1()) + ".mp4"

        clip = moviepy.VideoFileClip(output_video)
        clip.write_videofile(output_2)

        video_output = open(output_2, 'rb')
        video_bytes_output = video_output.read()
        st.video(video_bytes_output)





