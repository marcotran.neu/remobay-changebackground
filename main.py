# import pixellib
# from pixellib.tune_bg import alter_bg
#
# change_bg = alter_bg()
# change_bg.load_pascalvoc_model("deeplabv3_xception_tf_dim_ordering_tf_kernels.h5")
# change_bg.change_bg_img(f_image_path="co-gai-dep2-20170714170649.jpeg", b_image_path="download (5).jpeg", output_image_name="new_img.jpg")

import pixellib
from pixellib.tune_bg import alter_bg

change_bg = alter_bg(model_type="pb")
change_bg.load_pascalvoc_model("xception_pascalvoc.pb")
change_bg.change_video_bg("data/84a0d652-794a-11ec-8281-acde48001122.mp4", "data/892efc1c-794a-11ec-8281-acde48001122.jpg", frames_per_second=10, output_video_name="output_video.mp4", detect="person")